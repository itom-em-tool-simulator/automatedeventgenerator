Sources=('vRealize' 'TrueSight' 'Prometheus' 'AppDynamics' 'MotaData')
Source=${Sources[$(($RANDOM%5))]}
nodes=('10.2.50.1' '10.2.30.1' '10.2.40.1' '172.31.45.237' '208.187.161.1')
node=${nodes[$(($RANDOM%5))]}
metrics=('Disk Space Low' 'CPU Utilization High' 'Memory Utilization High' 'Network Usage Packets Dropped' 'DB Allocated Size Reached(MB)')
choice=$(($RANDOM%5))
metric_name=${metrics[$choice]}

d1=('Disk space low on node' $node '. Only' $(($(($RANDOM%9))+1)) '% Of Disk Space Left')
d0=('High CPU usage observed on server' $node '. CPU Utilization:' $(($(($RANDOM%10))+90)) '%')
d2=('Memory utilization exceeding threshold 90%. New processes getting killed automatically. Mem Util:' $(($(($RANDOM%10))+90)) '%')
d3=($(($(($RANDOM%20))+50)) '% packets to the server are getting dropped due to High Network Traffic.')
d4=('High Number of Active Connections on DB. Connections exceed 5000')
case $choice in
	0) description=${d0[@]}
	;;
	1) description=${d1[@]}
	;;
	2) description=${d2[@]}
	;;
	3) description=${d3[@]}
	;;
	4) description=${d4[@]}
	;;
esac
severity=$(($RANDOM%4))

payload='{"records":[{"source":"'$Source'", "node": "'$node'", "metric_name":"'$metric_name'", "severity":"'$severity'", "description":"'$description'", "additional_info": {"prom-severity": "High", "metric_value":"'$(($RANDOM%5000))'", "os_type":"Windows.Server.2008"}}]}'
echo $payload

curl -u "InmorphisAdmin":"Inmorphis123" -H "Content-Type: application/json" -d "$payload" "https://inmorphisservicespvtltddemo20.service-now.com/api/global/em/jsonv2"
